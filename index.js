const express = require("express")
const app = express()
require("dotenv").config()

const PORT = process.env.PORT || 3001

//GET: http://localhost:3000/
app.get("/", (req, res) => {
  return res.status(200).json({ message: "GET request to the Home page!" })
})

//POST: http://localhost:3000/
app.post("/", (req, res) => {
  return res.status(201).json({ message: "POST request to the Home page!" })
})

//POST: http://localhost:3000/
app.put("/", (req, res) => {
  return res.status(200).json({ message: "PUT request to the Home page!" })
})

//DELETE: http://localhost:3000/
app.delete("/", (req, res) => {
  return res.status(200).json({ message: "DELETE request to the Home page!" })
})

app.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}`)
})
